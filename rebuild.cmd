git clean -d -f -x
nuget restore
msbuild gtools.sln -t:Build -m:48 -p:Configuration=Debug -noConsoleLogger -detailedSummary -fl -interactive:False -restore
msbuild gtools.sln -t:Build -m:48 -p:Configuration=Release -noConsoleLogger -detailedSummary -fl -interactive:False -restore

