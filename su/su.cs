﻿using System;
using System.Security.Principal;
using System.Diagnostics;
using System.IO;
using ngbt.common;
using System.Text;
using System.Runtime.Versioning;

namespace su
{
    [SupportedOSPlatform("windows")]
    class su
    {
        private static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        /// <summary>
        /// This program finds an executable on the PATH. It can also find other stuff on the path, but 
        /// mostly it finds the executable.s
        /// </summary>
        /// <param name="args"></param>
        private void Run(string[] args)
        {
            Console.OutputEncoding = Encoding.GetEncoding(Encoding.Default.CodePage);

            if (IsAdministrator())
            {
                Console.WriteLine("You are already running with Administrator privileges - nothing to do...");
                return;
            }

            // we're using a special handling here, because we want to support some shortcuts.
            // for example: 
            // - "su d:\Projects" should open a cmd.exe in that folder
            // - "su" (no arguments) should open cmd.exe in the current folder
            // - "su regedit" should open regedit.exe with admin privileges

            foreach (string arg in args)
            {
                if (arg == "/?")
                {
                    Console.WriteLine($"su - Version {AppVersion.Get()}");
                    Console.WriteLine("Enable admin privileges everywhere - Copyright (C) 2020 by Gerson Kurz (https://p-nand-q.com)");
                    Console.WriteLine("");
                    Console.WriteLine("USAGE: su.exe [OPTIONS]");
                    Console.WriteLine("OPTIONS:");
                    Console.WriteLine("<Directory> .... open cmd.exe in that directory, with admin privileges");
                    Console.WriteLine("<Executable> ... open executable in current directory, with admin privileges");
                    Console.WriteLine("................ if you give no options, start cmd.exe in the current directory, with admin privileges");

                    return;
                }
            }

            if (args.Length == 0)
            {
                StartCmd(Environment.CurrentDirectory);
            }
            else
            {
                if (Directory.Exists(args[0]))
                {
                    StartCmd(args[0]);
                }
                else if (ProcessInfoTools.FindExecutable(args[0], out string exe))
                {
                    var startInfo = new ProcessStartInfo(exe)
                    {
                        Verb = "runas",
                        UseShellExecute = true,
                        WorkingDirectory = Environment.CurrentDirectory
                    };
                    Process.Start(startInfo);
                }
                else
                {
                    Console.WriteLine("ERROR: you must pass either no arguments, or a folder name, or the name of an executable.");
                }
            }
        }

        private void StartCmd(string folder)
        {
            var startInfo = new ProcessStartInfo(Path.Combine(Environment.SystemDirectory, "cmd.exe"))
            {
                Verb = "runas",
                WorkingDirectory = folder,
                UseShellExecute = true,
                Arguments = $"/K \"cd /d {folder}\""
            };
            Process.Start(startInfo);
        }

        static void Main(string[] args)
        {
            new su().Run(args);
        }
    }
}
