﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// 
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Serilog;

namespace ngbt.common
{
    public static class Tools
    {
        public static ILogger StaticLogger;

        public static bool Is64BitProcess
        {
            get
            {
                return IntPtr.Size == 8;
            }
        }

        public static bool DeleteFilesRecursive(string folder)
        {
            var dir = new DirectoryInfo(folder);
            bool success = true;
            foreach (var fi in dir.GetFiles())
            {
                try
                {
                    File.SetAttributes(fi.FullName, FileAttributes.Normal);
                    fi.Delete();
                }
                catch(Exception e)
                {
                    StaticLogger.Fatal(e, "Failed to remove file {0}", fi.FullName);
                    success = false;
                }
            }

            foreach (var di in dir.GetDirectories())
            {
                if( !DeleteFilesRecursive(di.FullName) )
                {
                    success = false;
                }
            }

            try
            {
                File.SetAttributes(folder, FileAttributes.Normal);
                Directory.Delete(folder);
            }
            catch (Exception e)
            {
                StaticLogger.Fatal(e, "Failed to remove folder {0}", folder);
                success = false;
            }
            return success;
        }

        public static string ShortenName(string input, Dictionary<string, string> lookup = null)
        {
            // Step 1: Environment variables
            foreach (var name in new string[] { "ProgramFiles(x86)", "ProgramFiles", "ProgramData" })
            {
                var val = Environment.GetEnvironmentVariable(name);
                if (val != null)
                {
                    if (input.StartsWith(val, StringComparison.OrdinalIgnoreCase))
                    {
                        if (input[val.Length] == '\\')
                        {
                            return $"%{name}%{input[val.Length ..]}";
                        }
                    }
                }
            }

            // step 2: replacement variables
            if (lookup != null)
            {
                foreach (var key in lookup.Keys)
                {
                    var value = lookup[key];
                    input = input.Replace(value, key);
                }
            }
            return input;
        }

        public static string ExpandName(string input, Dictionary<string, string> lookup = null)
        {
            if(lookup != null)
            {
                if(input.Contains("$$"))
                {
                    foreach(var key in lookup.Keys)
                    {
                        var value = lookup[key];
                        input = input.Replace(key, value);
                    }
                }
            }

            return Environment.ExpandEnvironmentVariables(input);
        }

        public static string WildCardToRegular(string value)
        {
            return "^" + Regex.Escape(value).Replace("\\?", ".").Replace("\\*", ".*") + "$";
        }

        
    }
}
