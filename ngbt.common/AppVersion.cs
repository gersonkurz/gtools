﻿using System.Reflection;

namespace ngbt.common
{
    public static class AppVersion
    {
        public static string Get()
        {
            string appName = Assembly.GetEntryAssembly().Location;
            AssemblyName assemblyName = AssemblyName.GetAssemblyName(appName);

            string[] tokens = assemblyName.Version.ToString().Split('.');
            return $"{tokens[0]}.{tokens[1]}";
        }
    }
}
