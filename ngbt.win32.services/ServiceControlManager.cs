﻿// Copyright (c) 2013, Gerson Kurz
// Copyright (C) 2020, NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Serilog;
using ngbt.common;
using System.Threading;

namespace ngbt.win32.services
{
    public class ServiceControlManager : IDisposable
    {
        internal IntPtr Handle;
        private readonly ILogger Log;

        public ServiceControlManager()
            : this(Environment.MachineName)
        {
        }
        
        public bool IsValid
        {
            get
            {
                if (Tools.Is64BitProcess)
                {
                    return Handle.ToInt64() != 0;
                }
                else
                {
                    return Handle.ToInt32() != 0;
                }
            }
        }

        public ServiceControlManager(string machineName)
        {
            Log = Tools.StaticLogger.ForContext("Context", ToString());

            if (string.IsNullOrEmpty(machineName))
                machineName = Environment.MachineName;

            Handle = NativeServiceAPI.OpenSCManager(
                machineName,
                NativeServiceAPI.SERVICES_ACTIVE_DATABASE,
                (uint)(ACCESS_MASK.STANDARD_RIGHTS_READ | ACCESS_MASK.GENERIC_READ));
            
            if(!IsValid)
            {
                Log.WindowsError("OpenSCManager() failed");
            }
        }

        #region IDisposable Members
        protected virtual void Dispose(bool disposing)
        {
            if (IsValid)
            {
                if (!NativeServiceAPI.CloseServiceHandle(Handle))
                {
                    Log.Warning("Unable to close ServiceControlManager.Handle");
                }
                Handle = new IntPtr(0);
            }
        }

        ~ServiceControlManager()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public ENUM_SERVICE_STATUS_PROCESS GetCurrentState(string serviceName)
        {
            foreach(var essp in EnumServiceStatusList())
            {
                if(essp.ServiceName.Equals(serviceName, StringComparison.OrdinalIgnoreCase))
                {
                    return essp;
                }
            }
            return null;
        }


        public bool Uninstall(string serviceName)
        {
            try
            {
                using var ns = new Service(
                        this, serviceName,
                        ACCESS_MASK.SERVICE_CHANGE_CONFIG | ACCESS_MASK.SERVICE_QUERY_STATUS | ACCESS_MASK.DELETE);
                if (NativeServiceAPI.DeleteService(ns.Handle))
                {
                    return true;
                }
                Log.WindowsError($"DeleteService({serviceName}) failed");
                return false;
            }
            catch (Exception e)
            {
                Log.Fatal(e, "DeleteService({0}) caught an exception", serviceName);
                return false;
            }
        }
        public bool Shutdown(string serviceName)
        {
            return Process(serviceName, new RequestServiceStop());
        }
        
        public bool Startup(string serviceName)
        {
            return Process(serviceName, new RequestServiceStart());
        }

        public bool Restart(string serviceName)
        {
            return Process(serviceName, new RequestServiceRestart());
        }
        
        public bool Pause(string serviceName)
        {
            return Process(serviceName, new RequestServicePause());
        }
        
        public bool Continue(string serviceName)
        {
            return Process(serviceName, new RequestServiceContinue());
        }

        private bool Process(string serviceName, ServiceStateRequest ssr)
        {
            try
            {
                var essp = GetCurrentState(serviceName);
                if(essp == null)
                {
                    Log.Error("ServiceName {0} not configured", serviceName);
                    return false;
                }

                Log.Information("Service {0} [{1}] is in state {2}", serviceName, essp.DisplayName, essp.CurrentState);

                if (ssr.HasSuccess(essp.CurrentState))
                {
                    Log.Information("Already in target status, done...");
                    return true;
                }

                var serviceAccessMask = ssr.GetServiceAccessMask() | ACCESS_MASK.STANDARD_RIGHTS_READ | ACCESS_MASK.SERVICE_QUERY_STATUS;
                if (essp.CurrentState == SC_RUNTIME_STATUS.SERVICE_STOPPED)
                {
                    serviceAccessMask &= ~(ACCESS_MASK.SERVICE_STOP);
                }

                bool requestedStatusChange = false;
                bool result = false;
                using var service = new Service(this, serviceName, serviceAccessMask);
                using var ss = new ServiceStatus(service);
                for (int i = 0; i < 100; ++i)
                {
                    if (!ss.Refresh())
                        break;

                    Log.Information("Service {0} [{1}] is in state {2}",
                        serviceName,
                        essp.DisplayName,
                        ss.Status.CurrentState);

                    if (ssr.HasSuccess(ss.Status.CurrentState))
                    {
                        Log.Information("Reached target status, done...");
                        result = true;
                        break;
                    }

                    // if we haven't asked the service to change its status yet, do so now. 
                    if (!requestedStatusChange)
                    {
                        requestedStatusChange = true;
                        Log.Information("Ask {0} to issue its status request on {1}", ssr, ss);
                        if (!ssr.Request(ss))
                        {
                            Log.WindowsError("Failed to control service status");
                            break;
                        }
                    }
                    else if (ssr.HasFailed(ss.Status.CurrentState))
                    {
                        Log.Error("ERROR, target state is one of the failed ones :(");
                        break;
                    }
                    Thread.Sleep(500);
                }
                return result;
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Unable to perform {0} on {1}", ssr, serviceName);
                return false;
            }
        }

        public HashSet<string> GetServiceNames(bool caseSensitive = false)
        {
            var result = new HashSet<string>();
            foreach(var essp in EnumServiceStatusList())
            {
                string key = essp.ServiceName;
                if (!caseSensitive)
                    key = key.ToUpper();
                result.Add(key);
            }
            return result;
        }

        public IEnumerable<ENUM_SERVICE_STATUS_PROCESS> EnumServiceStatusList(SC_SERVICE_TYPE ServiceType = SC_SERVICE_TYPE.SERVICE_WIN32)
        {
            // Quote from MSDN: Windows Server 2003 and Windows XP:  
            // The maximum size of this array is 64K bytes. This limit
            // was increased as of Windows Server 2003 SP1 and Windows XP SP2.

            const int BytesAllocated = 63 * 1024;

            IntPtr lpServices = Marshal.AllocHGlobal((int)BytesAllocated);
            int cbBytesNeeded = 0;
            int ServicesReturned = 0;
            int ResumeHandle = 0;
            bool repeat = true;
            while (repeat)
            {
                if (NativeServiceAPI.EnumServicesStatusEx(Handle,
                        SC_ENUM_TYPE.SC_ENUM_PROCESS_INFO,
                        ServiceType,
                        SC_QUERY_SERVICE_STATUS.SERVICE_STATE_ALL,
                        lpServices,
                        BytesAllocated,
                        ref cbBytesNeeded,
                        ref ServicesReturned,
                        ref ResumeHandle,
                        null))
                {
                    Log.Information("Got {0} services in last chunk", ServicesReturned);
                    repeat = false;
                }
                else
                {
                    int LastError = Marshal.GetLastWin32Error();
                    if (LastError == NativeServiceAPI.ERROR_MORE_DATA)
                    {
                        Log.Information("Got {0} services in this chunk", ServicesReturned);
                    }
                    else
                    {
                        Log.WindowsError("EnumServicesStatusEx()");
                        break;
                    }
                }

                long lPtr;
                if (Tools.Is64BitProcess)
                {
                    lPtr = lpServices.ToInt64();
                }
                else
                {
                    lPtr = lpServices.ToInt32();
                }
                for (int i = 0; i < ServicesReturned; i++)
                {
                    var essp = (ENUM_SERVICE_STATUS_PROCESS)
                        Marshal.PtrToStructure(
                            new IntPtr(lPtr),
                            typeof(ENUM_SERVICE_STATUS_PROCESS));

                    yield return essp;
                    lPtr += Marshal.SizeOf(essp);
                }
            }
        }

    }
}
